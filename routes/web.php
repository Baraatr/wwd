<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');
Route::get('/ratings', 'IndexController@ratings');
Route::get('/contact', 'ContactController@contact');
Route::get('/search', 'FullTextSearchController@index');
Route::get('/add', 'StoreController@add')->middleware('auth');
Route::post('/store', 'StoreController@store')->name('store');
Route::get('/shop', 'CartController@shopIndex')->middleware('auth');
Route::get('/estate/{id}', 'IndexController@getEstate');

Route::get('/addCart/{id}', 'CartController@addCart')->name('addCart');
Route::get('/checkout/{id}', 'CartController@checkout')->name('checkout')->middleware('auth');
Route::post('/checkout/{id}', 'CartController@checkout')->name('checkout')->middleware('auth');


Auth::routes();
Route::get('/login','IndexController@login')->name('login');
