<!DOCTYPE html>
<html>
<head>
    <title>Estates For All </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Oswald&display=swap" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <meta charset="utf-8">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light ">

        <a class=" navbar-brand" href="{{asset('/')}}">Estates For All</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('add')}}">Add product</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{asset('contact')}}">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{asset('search')}}">Search</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{asset('ratings')}}">What people say about us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{asset('shop')}}">Buy your house</a>

                </li>
                @guest

                @else

                <li class="nav-item"  >
                    <a class="nav-link " href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Log out</a>
                </li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                </form>

                @endguest
            </ul>
        </div>
    </nav>
    <div class="head">
        <img src="{{asset('images/checkot.png')}}">
    </div>
    <div class="container">

        <div style="margin-top: 70px;">
            <h3>Checkout</h3>
            <hr>
            <div class="row">
                <div class="col-lg-8 col-md-8" >
                    <form action="{{url('checkout',$estate->id)}}" method="POST" name="myform"  onsubmit="return validateform()">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Name" required="required">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Phone:</label>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone" required="required">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Branch:</label>
                                    <select class="form-control" name="branches"> 
                                        @foreach($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-send" value="Submit" style="color:white; background-color: #231f20 ; font-size: 18px;  padding: 10px; margin-top: 30px; border: 0px; width: 150px;">
                             </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br><br><br>
    <script type="text/javascript">
      function validateform(){  
          var phone=document.myform.phone.value;  
            
          if (isNaN(phone)){  
            alert("The phone must be a number.");  
            return false;  
          }
      }  
    </script>
</body>
</html>