<!DOCTYPE html>
<html>
<head>
    <title>Estates For All </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Oswald&display=swap" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <meta charset="utf-8">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light ">

      <a class=" navbar-brand" href="{{asset('/')}}">Estates For All</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="nav navbar-nav ">
          <li class="nav-item">
            <a class="nav-link" href="{{asset('add')}}">Add product</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{asset('contact')}}">Contact Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{asset('search')}}">Search</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{asset('ratings')}}">What people say about us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{asset('shop')}}">Buy your house</a>

          </li>
          @guest


          @else

          <li class="nav-item"  >
            <a class="nav-link " href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Log out</a>
          </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

          @endguest
        </ul>
      </div>
    </nav>
    <div class="head">
    <img src="{{asset('images/buy-house.jpg')}}">
    </div>
    <div class="container">
    
   <div style="margin-top: 50px;">
    <div class="row">
		@foreach($estates as $estate)
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="img-thumbnail images">
                <a href="/estate/{{ $estate->id }}"><img src="{{asset($estate->image)}}" width="100%"></a>
	                <h5><b>Address: </b>{{$estate->address}}</h5>
					        <span class="text"><b>Cladding: </b>{{$estate->cladding}}</span>
	                <p>Many modern and beautiful furnishings in many colors are located in all our branches.</p>
	                <b>Price:</b><span>{{$estate->price}}</span>
                  <a href="/checkout/{{ $estate->id }}" style="margin-left:180px" type="button" class="btn btn-success">Checkout</a>

                </div>
            </div>
        @endforeach
	</div>
   </div>
   
</div>
<br><br><br>

</body>
</html>