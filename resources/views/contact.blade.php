@extends('master')
@section('title')
    <p><b>Contact</b></p> 
@stop
@section('content')
<div class="content">
	<h3>Contact Us</h3>
	<hr>
	<div class="text-left">
              <div class="row fa-div">
        
                  <div class="col-3">
                      <h5>Phone: </h5>
                  </div>
                  <div class="col-4">
                      <h6>0993130613</h6>
                  </div>
              </div> 
              <div class="row fa-div">
                  <div class="col-3">
                      <h5>Address: </h5>
                  </div>
                  <div class="col-4">
                      <h6 >Mazzeh</h6>
                  </div>
              </div>           
          </div>
	<div class="row text-center">
        <div class="col-lg-12 col-lg-offset-2">
          
            <form id="contact-form" action="mailto:baraa.tr1998@gmail.com" method="post" enctype="text/plain">
                <div class="messages"></div>
                <div class="controls" style="margin-top:50px; ">
                    <div class="row">
                        
                        <div class="col-6">
                            <div class="form-group">
                                <input id="form_email" type="text" name="name" class="form-control" placeholder="Name" required="required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>   
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea id="form_message" name="message" class="form-control" placeholder="Message" rows="10" ></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <input type="submit" class="btn btn-send" value="Send message" style="color:white; background-color: #231f20 ; font-size: 18px;	padding: 10px; margin-top: 30px; border: 0px; width: 150px;">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>	
@stop
