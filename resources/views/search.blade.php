@extends('master')
@section('title')
    <p><b>Search for estates</b> </p> 
@stop
@section('content')

    <div class="content">
        <h3>Search for estates</h3>
        <hr>
      <form method="GET" action="{{ url('search') }}">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" name="search" class="form-control" placeholder="Search (ex: Malki 600 6 )">
                </div>
                <div class="col-md-6">
                    <button class="btn btn-success">Search</button>
                </div>
            </div>
        </form>
   <br/>
      <table class="table table-bordered">
            <tr>
                <th>Image</th>
                <th>Address</th>
                <th>Space</th>
                <th>Rooms</th>
                <th>Price</th>
                <th>Floor</th>
                <th>Cladding</th>
            </tr>
            @if($estates->count())
                @foreach($estates as $estate)
                <tr>
                    <td><img src="{{ $estate->image }}" width="150px" height="100px"></td>
                    <td>{{ $estate->address }}</td>
                    <td>{{ $estate->space }}</td>
                    <td>{{ $estate->room }}</td>
                    <td>{{ $estate->price }}</td>
                    <td>{{ $estate->floor }}</td>
                    <td>{{ $estate->cladding }}</td>
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="3" class="text-danger">Result not found.</td>
            </tr>
            @endif
        </table>


    </div>
@stop