@extends('master')
@section('title')
    <p><b>Estate For All</b> </p> 
@stop
@section('content')
<div class="content">
	<h3>About</h3>
	<hr>
	<div class="row">
		
		<div class="col-lg-6 col-md-12 col-ls-12 col-ms-12">
			<img src="{{asset('images/10.jpg')}}" style="width: 100%;">
		</div>
		<div class="col-lg-6 col-md-12 col-ls-12 col-ms-12">
			<div class="about">
				<h4>About Us</h4>
				<p>The Estates For All site allows you as a customer to view and buy the available estate in the web site.</p>
		        <p>What services does our company offer?</p>
			    <p># Through our website, you can buy a home without any search efforts. </p>
			    <p># You can find the best sales offer for estates in our website.</p>
			    <p># In the end, our website aims to spread the culture of electronic marketing.</p>
			    
			</div>
		</div>
	</div>
	<br><br>
	<h3>Our Estates</h3>
	<hr>
		<div class="row">
			@foreach($estates as $estate)
			<div class="col-lg-4 col-md-6 col-ls-2 col-ms-12">
				<div class="img">
				    <img src="{{$estate->image}}" class="" width="350" height="300"> 
					<a href="/estate/{{ $estate->id }}"><span>{{$estate->address}}</span></a>
				</div>
			</div>
			@endforeach
		</div>	
		<br><br>
	<h3>Our Branches </h3>
	<hr>
	<div class="row">
		@foreach($branches as $branch)
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="img-thumbnail images">
	                <img src="{{asset($branch->image)}}" width="100%">
	                <h5>{{$branch->name}}</h5>
					<span class="text">{{$branch->address}}</span>
	                <p>Many modern and beautiful houses are located in all our branches.</p>
	                <b>Phone:</b><span> +963{{$branch->phone}}</span>
                </div>
            </div>
        @endforeach
	</div>

</div>

@stop
