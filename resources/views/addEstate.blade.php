@extends('master')
@section('title')
    <p><b>Add Estate</b> </p> 
@stop
@section('content')
<div class="content">
<h3>Add New Estate</h3>
<hr>
    <form method="POST" action="{{Route('store')}}"  enctype="multipart/form-data" name="myform" onsubmit="return validateform()" >
        @csrf
        <div class="row ">
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="text" name="address" class="form-control" placeholder="address" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="text" name="space" class="form-control" placeholder="space" required>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <select class="form-control" name="cladding"> 
                    <option value="1">medium</option>
                    <option value="2">delux</option>
                    <option value="3">super delux</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="number" name="floor" class="form-control" placeholder="floor" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="number" name="room" class="form-control" placeholder="rooms" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <select class="form-control" name="branches"> 
                        @foreach($branches as $branch)
                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="text" name="price" class="form-control" placeholder="price" required>
                    @if ($errors->has('price'))
                        <span class="help-block alert-danger">
                            {{ $errors->first('price') }}
                        </span>
                    @endif 
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-ls-12">
                <div class="form-group">
                    <input type="file" name="cover" class="form-control" style="height: 40px;" required>
                    
                </div>
            </div>            
        </div> 

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <br>
                    <label>Choose multiple image</label>

                    <input type="file" name="images[]" class="form-control" style="height: 43px;" multiple required id="gallery-photo-add">
                    <div class="gallery"></div>

                </div>
            </div>
        </div>    

            <input type="Submit" value="Save" class="btn" style="color:white; background-color: #231f20 ; font-size: 18px;  padding: 10px; margin-top: 30px; border: 0px; width: 100px;">
            <div style="clear: both;"></div>
        
    </form>
</div>
<script>  
function validateform(){  
    var price=document.myform.price.value;  
      
    if (isNaN(price)){  
      alert("The price must be a number.");  
      return false;  
    }
}  
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img width="100px" height="100px" style="padding-top:30px; margin-right:20px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script> 	
@stop
