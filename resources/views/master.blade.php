<!DOCTYPE html>
<html>
<head>
	<title>Estates For All </title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" ></script>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Oswald&display=swap" rel="stylesheet">
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<meta charset="utf-8">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light ">

	  <a class=" navbar-brand" href="{{asset('/')}}">Estates For All</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarResponsive">
	    <ul class="nav navbar-nav ">
	      <li class="nav-item">
	        <a class="nav-link" href="{{asset('add')}}">Add Estate</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " href="{{asset('contact')}}">Contact Us</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " href="{{asset('search')}}">Search</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " href="{{asset('ratings')}}">What people say about us</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " href="{{asset('shop')}}">Buy your house</a>

	      </li>
	      @guest


	      @else

	      <li class="nav-item"  >
	        <a class="nav-link " href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Log out</a>
	      </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

	      @endguest
	    </ul>
	  </div>
	</nav>
<div class="head">
	<img src="{{asset('images/2.jpg')}}">
	@yield('title')
</div>
<div class="container">
	
@yield('content')
	
</div>
	<footer>
	  <div class="container">
	      <div class="row">
	      
		      <div class="col-lg-4 col-md-6">
		          <h3>Our Mission</h3>
		          <p>allows you as a customer to view and buy the available estate in the web site.</p>
		        
		      </div>
		      
		      <div class="col-lg-4 col-md-6">
		       
		       	  <h3>Our Branches</h3> 
		          <ul>
			          <li><a>Estates For All 1</a> </li> 
			          <li><a>Estates For All 2</a> </li> 
			          <li><a>Estates For All 3</a> </li> 
		          </ul>
		      </div>
		      
		      <div class="col-lg-4">
		          <h3>Gallery</h3>
		          <img class="img-thumbnail" src="{{asset('21034226981619897037275158845.jpg')}}" alt="" />
		          <img class="img-thumbnail" src="{{asset('images/22.jpg')}}" alt="" />
		          <img class="img-thumbnail" src="{{asset('images/33.jpg')}}" alt="" />
		          <img class="img-thumbnail" src="{{asset('images/44.jpg')}}" alt="" />
		      </div> 
	      </div>
	  </div>
	  <div class="copyright text-center">
	    Copyright &copy; 2020 <span>Estates For All</span>
	  </div>
	</footer>
</body>
</html>