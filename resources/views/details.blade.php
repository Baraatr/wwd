<!DOCTYPE html>
<html>
<head>
  <title>Estates For All </title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/lightslider.css')}}">
  <link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Oswald&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
  <script type="text/javascript" src="{{asset('js/lightslider.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/modernizr.custom.53451.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
  <meta charset="utf-8">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light ">

    <a class=" navbar-brand" href="{{asset('/')}}">Estates For All</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class=" navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{asset('add')}}">Add product</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="{{asset('contact')}}">Contact Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="{{asset('search')}}">Search</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="{{asset('ratings')}}">What people say about us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="{{asset('shop')}}">Buy your house</a>

        </li>
        @guest


        @else

        <li class="nav-item"  >
          <a class="nav-link " href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Log out</a>
        </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        @endguest
      </ul>
    </div>
  </nav>
<div class="head">
  <div class="banner">

        <ul id="lightSlider">
          @foreach($estates->galleries as $photo)

            <li data-thumb="{{asset($photo->name)}}">
                <img src="{{asset($photo->name)}}" />
            </li>
          @endforeach
        </ul>
                  

    </div>
</div>
<div class="container">
    <br><br>
    <h3>Address</h3>
    <hr>
    <p>{{$estates->address}}</p>
    <br><br>
    <h3>Floor</h3>
    <hr>
    <span>{{$estates->floor}}</span>
    <br><br>
    <h3>Space</h3>
    <hr>
    <span>{{$estates->space}}</span>
    <br><br>
    <h3>Cladding</h3>
    <hr>
    <span>{{$estates->cladding}}</span>
    <br><br>
    <h3>Room number</h3>
    <hr>
    <span>{{$estates->room}}</span>
    <br><br>
    <h3>Price</h3>
    <hr>
    <span>{{$estates->price}}</span>
    <br><br>
    <a href="/checkout/{{ $estates->id }}" type="submit"   class="btn" style="color:white; background-color: #231f20 ; font-size: 18px;  padding: 10px; margin-top: 30px; border: 0px; width: 120px;" >Checkout</a>

</div>




  <footer>
    <div class="container">
        <div class="row">
        
          <div class="col-lg-4 col-md-6">
              <h3>Our Mission</h3>
              <p> allows you as a customer to view and buy the available estate in the web site.</p>
            
          </div>
          
          <div class="col-lg-4 col-md-6">
           
              <h3>Our Sales halls</h3> 
              <ul>
                <li><a>Estates For All 1</a> </li> 
                <li><a>Estates For All 2</a> </li> 
                <li><a>Estates For All 3</a> </li> 
              </ul>
          </div>
          
          <div class="col-lg-4">
              <h3>Gallery</h3>
              <img class="img-thumbnail" src="{{asset('images/11.jpg')}}" alt="" />
              <img class="img-thumbnail" src="{{asset('images/22.jpg')}}" alt="" />
              <img class="img-thumbnail" src="{{asset('images/33.jpg')}}" alt="" />
              <img class="img-thumbnail" src="{{asset('images/44.jpg')}}" alt="" />
          </div> 
        </div>
    </div>
    <div class="copyright text-center">
      Copyright &copy; 2020 <span>Estates For All</span>
    </div>
  </footer>
  <script type="text/javascript">
        $(document).ready(function() {
          $("#lightSlider").lightSlider({
              item: 1,
              auto:true,
              gallery: true,
              loop:true,
              speed:1000,
              pause:4000
        });
        });
    </script>
</body>
</html>