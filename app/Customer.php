<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function orders(){
    	return $this->hasMany(Order::class)->select('id','color','total','customer_id','sales_hall_id');
    }
}
