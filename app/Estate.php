<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Estate extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
	protected $searchable = [
       
        'columns' => [
            'estates.address'    => 10,
            'estates.space'      => 10,
            'estates.room'       => 10,
            'estates.price'      => 10,
            'estates.floor'      => 10,
            'estates.cladding'   => 10,
            'estates.id'         => 10
            
        ],
        
    ];
     protected $fillable = [
        'address', 'space', 'rooms','price','floor','cladding',
    ];

    public function galleries(){
    	return $this->hasMany(Gallery::class)->select('id','name','estate_id');
    }
    public function ratings(){
    	return $this->hasMany(Rating::class)->select('id','message','estate_id','customer_id');
    }

    public function branches(){
        return $this->hasMany(Branch::class)->select('id','name');
    }

}
