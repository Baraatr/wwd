<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function products(){
    	return $this->belongsTo(Product::class ,'product_id');
    } 
}
