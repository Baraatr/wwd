<?php

namespace App;


class Cart 
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart){

        if($oldCart){
            $this->items =$oldCart->items;
            $this->totalPrice =$oldCart->totalPrice;
            $this->totalQty =$oldCart->totalQty;

        }
    }
    public function add($item ,$price,$id){
        $storedItem = ['qty' => 0 , 'price' => $price , 'item' => $item];
        if($this->items){
            if(array_key_exists($id, $this->items)){
                $storedItem =$this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['price'] = $price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        
        $this->totalQty++;
        $this->totalPrice += $price;
    }
    public function remove($id) {

        if(!$this->items || !isset($this->items[$id])) {
            return false;  // maybe throw an exception here?
        }
        $this->totalQty -= $this->items[$id]['qty'];
        $this->shippingCost = ($this->totalQty * 1) + 1;
        $this->totalPrice -=  $this->items[$id]['price'] ;
        $this->subTotal = $this->totalPrice + $this->shippingCost;

        unset($this->items[$id]);
    }
    public function updateItem($item, $id) {

        if($this->items[$id]['qty']>1){
        $this->items[$id]['qty'] = $this->items[$id]['qty'] -1;

        $this->items[$id]['price'] = $this->items[$id]['qty'] * $item->price;

        $this->totalQty -= 1 ;
        $this->totalPrice = $this->totalPrice - $item->price;

        // foreach($this->items as $element) {
        //     // $this->totalQty += $element['qty'];
        //     // DD($this->totalPrice);
        // }
        }
        else{
            if(!$this->items || !isset($this->items[$id])) {
            return false;  // maybe throw an exception here?
            }
            $this->totalQty -= $this->items[$id]['qty'];
            $this->shippingCost = ($this->totalQty * 1) + 1;
            $this->totalPrice -=  $this->items[$id]['price'] ;
            $this->subTotal = $this->totalPrice + $this->shippingCost;

            unset($this->items[$id]);
        }
        
    }
}
