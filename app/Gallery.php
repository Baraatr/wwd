<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function estates(){
    	return $this->belongsTo(Estate::class ,'estate_id');
    }
    protected $guarded = [];
	

}
