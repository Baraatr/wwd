<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function estates(){
    	return $this->belongsTo(Estate::class ,'estate_id');
    }
}
