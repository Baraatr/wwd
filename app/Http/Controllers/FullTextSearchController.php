<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estate;

class FullTextSearchController extends Controller
{
    

    public function index(Request $request)
    {
        if($request->has('search')){
            $estates = Estate::search($request->get('search'))->get()->where('is_sold','0');  
        }else{
            $estates = Estate::get()->where('is_sold','0');
        }
        return view('search', compact('estates'));
    }    
}
