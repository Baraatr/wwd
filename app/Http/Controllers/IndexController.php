<?php

namespace App\Http\Controllers;
use App\Branch;
use Auth;
use App\Estate;
use App\Rating;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    
    public function index(Request $request){
        $branches= Branch::select('branches.name','branches.id','branches.address','branches.image','branches.phone')->get();
        $estates= Estate::select('estates.address','estates.id','estates.image')->get();
        return view('index',compact("branches","estates"));
    }
    
    public function login(Request $request)
    {  
        return view('login');
    }
    
    public function getEstate(Request $request, $id){
        $estates = Estate::with('galleries')->where('id',$id)->get()->first();
        return view('details', compact('estates'));
    }
    
    public function ratings(Request $request)
    {  
        return view('ratings');
    }
}
