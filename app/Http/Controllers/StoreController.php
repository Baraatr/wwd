<?php

namespace App\Http\Controllers;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Gallery;
use App\Estate;
use App\Branch;

class StoreController extends Controller
{


    public function add(){
        $branches= Branch::select('branches.name','branches.id')->get();
        return view('addEstate',compact('branches'));
    }


    public function store(request $request) {
    	    // validate form 
        $this->validate(request(),[
            
            'price'       => 'numeric',
            
        ]);
    	$estate = new Estate();	
    	$estate->address = $request->address;
    	$estate->space = $request->space;
    	$estate->floor = $request->floor;
    	$estate->room = $request->room;
    	$estate->cladding = $request->cladding;
    	$estate->price = $request->price;
    	$estate->branches_id = $request->branches;
    	if (isset($request->cover) && $request->cover != NULL) {
            $image_name = rand().time().rand().'.'. $request->cover->getClientOriginalExtension();
            $estate->image = $image_name;
            Image::make($request->cover)->save($image_name);
        }
    	$estate->save();
	    $images;
	    if($files=$request->file('images')){
	        foreach($files as $file){
	            $name = rand().time().rand().'.'.$file->getClientOriginalName();
	            $file->move('image',$name);
	            $images=$name;
	            $photo= new Gallery();
			    $photo->name = 'image/'.$images;
			    $photo->estate_id =$estate->id;
			    $photo->save();
	        }
    }
    return redirect('/');
}
 		
}
