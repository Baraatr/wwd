<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Branch;
use App\Estate;
use App\Order;
use Session;
class CartController extends Controller
{

    public function shopIndex(){
    
		$estates= Estate::select('estates.address','estates.id','estates.image')->where('is_sold','0')->get();
        return view('shop',compact("estates"));
	}
	public function checkout(Request $request,$id){
		
		if($request->isMethod('get')){
			$estate =  Estate::find($id);
			
			$branches= Branch::select('branches.name','branches.id')->get();

			return view('checkout',compact('branches','estate'));
		}else{

			
		$estates = Estate::where('id',$id)->first();
		// dd($estates);
		if ($estates){$estates->is_sold=1;
			$estates->save();
		}
		// $sold = $estates->where('is_sold', '0')->first();
		// $sold->is_sold = '1';
		
		
    	$customer = new Customer();	
    	$customer->name = $request->name;
    	$customer->email = $request->email;
    	$customer->phone = $request->phone;
    	$customer->save();

        $order = new Order(); 
        $order->customer_id = $customer->id;
        $order->branch_id = $request->branches;
        $order->save();
    	return redirect()->to('/');
		}
	}
}
